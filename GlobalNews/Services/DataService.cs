﻿using GlobalNews.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace GlobalNews.Services
{
    public class DataService
    {
        JsonSerializerOptions _serializerOptions;
        public DataService()
        {
            _serializerOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true
            };
        }
        public async Task<ResponseResult> GetArticlesAsync(string url)
        {
            using (var client = new HttpClient())
            {

                client.DefaultRequestHeaders.Add("user-agent", "News-API-csharp/0.1");
                client.DefaultRequestHeaders.Add("x-api-key", App.token);

                ResponseResult result = null;

                Uri uri = new Uri(string.Format(url,string.Empty));
                try
                {
                    HttpResponseMessage response = await client.GetAsync(uri);
                    if (response.IsSuccessStatusCode)
                    {
                        string contentResult = await response.Content.ReadAsStringAsync();
                        result = JsonSerializer.Deserialize<ResponseResult>(contentResult, _serializerOptions);
                    }
                    return result;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
