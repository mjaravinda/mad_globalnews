using GlobalNews.ViewModels;

namespace GlobalNews.Views;

public partial class FullArticlePage : ContentPage
{
	public FullArticlePage(NewsVM vm)
	{
		InitializeComponent();
		this.BindingContext= vm;
	}
}