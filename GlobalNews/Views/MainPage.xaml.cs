using GlobalNews.Models;
using GlobalNews.ViewModels;
using System.Windows.Input;

namespace GlobalNews.Views;

public partial class MainPage : ContentPage
{
    private uint duration = 300u;

    NewsVM vm;
    
    public MainPage()
	{
        vm = new NewsVM(this);
		InitializeComponent();
        vm.LoadInitialData();
        this.BindingContext = vm;
       

    }

   
    private void MenuGrid_TapGestureRecognizer_Tapped(object sender, TappedEventArgs e)
    {
        HideMenu();

    }

    private void hamburger_Clicked(object sender, EventArgs e)
    {
        ShowMenu();
    }

    private async void Menu_CollectionChanged(object sender, SelectionChangedEventArgs e)
    {
        if (vm != null && vm.ArticleCategory != null)
        {
            vm.IsBusy = true;
            await vm.SelectItem();
            vm.IsBusy = false;
           
            HideMenu();
         
        }


    }

    public void ShowMenu() 
    {
        mainContent.TranslateTo(this.Width * 0.4, this.Height * 0.02, duration, Easing.CubicIn);
        mainContent.ScaleTo(0.8, duration);
        mainContent.RotateYTo(-10, duration, Easing.CubicIn);
        mainContent.FadeTo(0.8, duration);
    }

    public void HideMenu()
    {
        mainContent.FadeTo(1, duration);
        mainContent.RotateYTo(0, duration, Easing.CubicIn);
        mainContent.ScaleTo(1, duration);
        mainContent.TranslateTo(0, 0, duration, Easing.CubicIn);
    }


    private async void ArticleSelected(object sender, SelectionChangedEventArgs e)
    {
        vm.SelectedArticle =(Article)e.CurrentSelection.FirstOrDefault();
        await this.Navigation.PushAsync(new NewsContent(vm));
    }

    private void ImageButton_Clicked(object sender, EventArgs e)
    {

    }

    private void FilterMenuButtonClicked(object sender, EventArgs e)
    {
        vm.ShowFilter = !vm.ShowFilter;
    }





    //private async void SearchItemPressed(object sender, EventArgs e)
    //{
    //    if (!string.IsNullOrEmpty(vm.SearchText))
    //    {
    //        await vm.SelectItem();
    //        HideMenu();
    //    }

    //}
}