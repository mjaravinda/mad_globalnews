

using GlobalNews.ViewModels;

namespace GlobalNews.Views;

public partial class NewsContent : ContentPage
{
	NewsVM vm;
	public NewsContent(ViewModels.NewsVM _vm)
	{
		InitializeComponent();
		vm = _vm;
		this.BindingContext= vm;
		
	}

    private void ReadFullArticleClicked(object sender, EventArgs e)
    {
		this.Navigation.PushAsync(new FullArticlePage(vm));
    }
}