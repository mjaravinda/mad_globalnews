﻿namespace GlobalNews;

public partial class App : Application
{
	public static string token = "db6fa363626c4ce6963644184d02f45c";
	public static string baseURL = "https://newsapi.org/";
	public static string URLVersion = "v2/";
	public App()
	{
		InitializeComponent();

		MainPage = new NavigationPage(new Views.MainPage());
	}
}
