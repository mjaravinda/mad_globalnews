﻿using GlobalNews.Models;
using GlobalNews.Services;
using GlobalNews.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GlobalNews.ViewModels
{
    public class NewsVM: BaseVM
    {
        DataService service;
        public ICommand SearchCommand { get; set; }
        public ICommand SortCommand { get; set; }
        Views.MainPage page;
             
        public NewsVM(Views.MainPage p)
        {
            SearchCommand = new Command(SearchEvent);
            SortCommand = new Command(SortEvent);
            page = p;
            Sorting = "publishedAt";
            service = new DataService();
        }

        private async void SortEvent(object obj)
        {
            var url = $"{App.baseURL}{App.URLVersion}everything?q={searchText}";
            if (From.HasValue && To.HasValue)
                url += $"&from={From.Value.ToString("yyyy-MM-dd")}&to={to.Value.ToString("yyyy-MM-dd")}";

            if (string.IsNullOrEmpty(Sorting))
                Sorting = "publishedAt";

            url += $"&sortBy={Sorting}";

            await this.GetNewsAsync(url);
            this.ShowFilter = false;
        }

        private async void SearchEvent(object obj)
        {
            this.IsBusy = true;
            await this.SelectItem();
            this.IsBusy = false;
            page.HideMenu();
         

        }   

        private Article topNews;

        public Article TopNews
        {
            get { return topNews; }
            set { topNews = value; OnPropertyChanged();  }
        }

        private ObservableCollection<Article> news;

        public ObservableCollection<Article> News
        {
            get { return news; }
            set { news = value; OnPropertyChanged(); }
        }

        private List<ArticleCategory> categories;

        public List<ArticleCategory> Categories
        {
            get { return categories; }
            set { categories = value; OnPropertyChanged(); }
        }

        private List<Country> countries;

        public List<Country> Countries
        {
            get { return countries; }
            set { countries = value; OnPropertyChanged(); }
        }

        private Country country;

        public Country Country
        {
            get { return country; }
            set { country = value; OnPropertyChanged(); }
        }


        private int recordCount;

        public int RecordCount
        {
            get { return recordCount; }
            set { recordCount = value; OnPropertyChanged(); }
        }



        private List<Language> languages;

        public List<Language> Lanaguages
        {
            get { return languages; }
            set { languages = value; OnPropertyChanged(); }
        }

        private Language language;

        public Language Language
        {
            get { return language; }
            set { language = value; OnPropertyChanged(); }
        }

        private ArticleCategory articleCategory;

        public ArticleCategory ArticleCategory
        {
            get { return articleCategory; }
            set { articleCategory = value; OnPropertyChanged(); }
        }

        private string searchText;
                
        public string SearchText
        {
            get { return searchText; }
            set { searchText = value;
                if (string.IsNullOrEmpty(value))
                    ShowFilterButton = false;
                else
                    ShowFilterButton= true;
                OnPropertyChanged(); 
            }
        }

        private string currentURL;

        public string CurrentURL
        {
            get { return currentURL; }
            set { currentURL = value; OnPropertyChanged(); }
        }

        private Article selectedArticle;

        public Article SelectedArticle
        {
            get { return selectedArticle; }
            set { selectedArticle = value; OnPropertyChanged(); }
        }

        private DateTime? from;

        public DateTime? From
        {
            get { return from; }
            set { from = value; OnPropertyChanged(); }
        }

        private DateTime? to;

        public DateTime? To
        {
            get { return to; }
            set { to = value; OnPropertyChanged(); }
        }

        private String sorting;

        public String Sorting
        {
            get { return sorting; }
            set { sorting = value; OnPropertyChanged(); }
        }


        public List<string> Sortings { 
            get 
            {
                return new List<string> 
                {
                    "publishedAt","popularity","relevancy"
                };
                
            } 
        }

        private bool showFilter;

        public bool ShowFilter
        {
            get { return showFilter; }
            set { showFilter = value; OnPropertyChanged(); }
        }


        private bool showFilterButton;

        public bool ShowFilterButton
        {
            get { return showFilterButton; }
            set { showFilterButton = value; OnPropertyChanged(); }
        }


        public async void LoadInitialData() 
        {
            Categories = ArticleCategory.ToList();
           // Category = Categories.FirstOrDefault();
            Countries = Country.ToList();
            Country = Countries.FirstOrDefault();
            Lanaguages = Language.ToList();
            Language = Lanaguages.FirstOrDefault();

           // ArticleCategory = Categories.FirstOrDefault();
            await this.SelectItem();
        }


        private async Task GetNewsAsync(string url)
        {
            try
            {
                var result = await service.GetArticlesAsync(url);
                if (result != null && result.Articles != null && result.Articles.Count > 0)
                {
                    RecordCount = result.Articles.Count;
                    News = new ObservableCollection<Article>(result.Articles);
                    TopNews = News.FirstOrDefault();
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }


        public async Task SelectItem()
        {            
         
            if (ArticleCategory ==null || ArticleCategory.DisplayName.Equals("Top Headlines"))
            {
                CurrentURL = $"{App.baseURL}{App.URLVersion}top-headlines?category=general&language=en";
            }          
            else
            {
                CurrentURL = $"{App.baseURL}{App.URLVersion}top-headlines?category={ArticleCategory.ParameeterValue}&language=en";
            }
            if (!string.IsNullOrEmpty(SearchText)) 
            {
                CurrentURL += $"&q={searchText}";
            }
            await this.GetNewsAsync(CurrentURL);
        }

       





    }
}
