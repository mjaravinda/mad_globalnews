﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Models
{
    public class ArticleCategory
    {
        public string DisplayName { get; set; }
        public string ParameeterValue
        {
            get
            {
                if (!string.IsNullOrEmpty(DisplayName))
                    return DisplayName.ToLower();
                else
                    return null;
            }
        }

        public static List<ArticleCategory> ToList()
        {
            return new List<ArticleCategory>
            {
                new ArticleCategory { DisplayName= "Top Headlines" },      
                new ArticleCategory { DisplayName= "Business" },
                new ArticleCategory { DisplayName= "Entertainment" },
                new ArticleCategory { DisplayName= "Health" },
      
                new ArticleCategory { DisplayName= "Science" },
                new ArticleCategory { DisplayName= "Sports" },
                new ArticleCategory { DisplayName= "Technology" },
            };

        }
    }
}
