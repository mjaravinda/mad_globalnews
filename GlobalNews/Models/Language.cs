﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GlobalNews.Models
{
    public class Language
    {
        public Language(string code, string name)
        {
            Code = code;
            Name = name;

        }

        public string Code { get; set; }
        public string Name { get; set; }


        public static List<Language> ToList()
        {
            return new List<Language>
            {
                new Language("EN","English"),
                new Language("JP","Japanese"),
                new Language("DE","German"),
                new Language("JP","Japan"),


            };
        }
    }
}
