﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Models
{
    public class Country
    {
        public Country(string code, string name)
        {
            Code = code;
            Name = name;
           
        }

        public string Code { get; set; }
        public string Name { get; set; }


        public static List<Country> ToList()
        {
            return new List<Country>
            {
                new Country("ALL","All Countries"),
                new Country("AU","Australia"),
                new Country("CA","Canada"),
                new Country("JP","Japan"),
                new Country("NO","Norway"),
                new Country("RU","Russia"),
                new Country("GB","UK"),
                new Country("US","US"),
         
            };
        }
    }
}
