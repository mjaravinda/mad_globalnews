﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Models
{
    public class ResponseResult
    {
      
        public string Status { get; set; }  
        public string TotalRecords { get; set; }     
        public List<Article> Articles { get; set; }
    }
}
